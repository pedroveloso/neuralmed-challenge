from nameko.rpc import rpc, RpcProxy

from business.image_resizer_business import resize_image

class ImageResizerService:
    name = "image_resizer_service"

    @rpc
    def resize_image(self, image):
        return resize_image(image)
