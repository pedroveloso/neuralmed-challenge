import numpy as np
import cv2
from PIL import Image

def resize_image(image_numpy_tolist, new_size = None, image_read_mode = None, resize_interpolation = None):

    if new_size is None:
        new_size = 384

    if image_read_mode is None:
        image_read_mode = cv2.IMREAD_UNCHANGED

    if resize_interpolation is None:
        resize_interpolation = cv2.INTER_LINEAR

    image_converted = np.array(image_numpy_tolist).astype('float32')
    resize_dimensions = (new_size, new_size)
    resized_image = cv2.resize(image_converted, resize_dimensions, resize_interpolation)
    resized_image_np = np.array(resized_image).tolist()

    return resized_image_np
