# API Gateway para upload de imagens + Broker + Microsserviço de redimensionamento

## Breve descrição do projeto
API Gateway(Flask) --> Broker(RabbitMQ) --> Serviço RPC (Nameko). As aplicações se comunicam através do broker RabbitMQ.

## Requisitos para rodar
Docker

## Para rodar o projeto
Executar `docker-compose up` na raiz desse projeto

## Para testar o envio das imagens
curl -F "image=@**CAMINHO COMPLETO ATÉ O ARQUIVO DE IMAGEM QUE PRETENDE ENVIAR**" localhost:5000/image/upload

## Extras (Respostas)
### Se o tamanho for parametrizável como você mudaria a sua arquitetura?
Com o uso de um Load Balancer, poderíamos direcionar cada request para grupo de recursos computacionais especificamente alocados de acordo com faixas para os tamanho desejados e/ou tamanho das imagens. Por exemplo, imagens maiores e/ou operações que desejassem redimensionamentos/técnicas mais complexos poderiam ter grupo de recursos focados para eles (por exemplo: mais máquinas ou máquinas mais potentes)

### Qual a complexidade da sua solução?
Em termos computacionais O(n) pois estou utilizando a interpolação linear para

### É possível melhorar a performance da solução? Como as melhorias impactam a leitura e manutenção do código?
Sim, a performance poderia melhorar com a paralelização da chamada de redimensionamento e possivelmente com a compactação dos arquivos transferidos na rede. Alguma refatoração poderia ser necessária mas o uso de testes auxiliaria nesse processo. No mais, o uso das boas práticas de programação visam garantir que os requisitos de legibilidade e manutenabilidade sejam alcançados.

### De que forma o sistema pode escalar com a arquitetura planejada?
A arquitetura proposta tem granularidade para escalar facilmente pois as partes podem aumentar/dimunir o consumo de recursos de maneira independente. No caso, a decomposição das 3 partes (API Gateway, Broker, Microsserviço) para serem geridas e monitoradas de maneira separada, permite que sejam escaladas conforme a necessidade de cada uma.
