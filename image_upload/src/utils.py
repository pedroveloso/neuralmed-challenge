import uuid

from flask_uploads import extension

from flask import current_app

from extensions import image_set


def save_image(image, folder):

    filename = '{}.{}'.format(uuid.uuid4(), extension(image.filename))
    image_set.save(image, folder=folder, name=filename)

    return filename
