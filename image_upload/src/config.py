class Config:
    DEBUG = True
    HOST='0.0.0.0'
    PORT = 5000
    AMQP_URI = 'amqp://guest:guest@rabbit:5672/'

    UPLOADED_IMAGES_DEST = 'static/images'
    IMAGE_MAX_SIZE = 10*1024*1024 # 10 MB
