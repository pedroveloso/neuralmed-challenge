from flask import Flask
from flask_restful import Api
from flask_uploads import configure_uploads, patch_request_class

from config import Config
from extensions import image_set

from services.image_upload_resource import ImageUploadResource

def create_app():
    app = Flask(__name__)
    app.config.from_object(Config)

    register_extensions(app)
    register_resources(app)

    return app

def register_extensions(app):
    configure_uploads(app, image_set)
    patch_request_class(app, Config.IMAGE_MAX_SIZE)

def register_resources(app):

    api = Api(app)
    api.add_resource(ImageUploadResource, '/image/upload')


if __name__ == '__main__':
    """Start Flask as API Gateway to serve mircoservices"""
    app = create_app()
    app.run(host=Config.HOST, port=Config.PORT, debug=Config.DEBUG)
