import os

from flask import request, url_for
from flask_restful import Resource
from http import HTTPStatus
from nameko.rpc import RpcProxy
from nameko.standalone.rpc import ClusterRpcProxy

from webargs import fields
from webargs.flaskparser import use_kwargs
from PIL import Image
import numpy as np

from extensions import image_set

from utils import save_image

class ImageUploadResource(Resource):

    def post(self):

        file = request.files.get('image')

        if not file:
            return {'message': 'Not a valid image'}, HTTPStatus.BAD_REQUEST

        if not image_set.file_allowed(file, file.filename):
            return {'message': 'File type not allowed'}, HTTPStatus.BAD_REQUEST

        filename = save_image(image=file, folder='images')

        with ClusterRpcProxy({'AMQP_URI': 'amqp://guest:guest@rabbit:5672/'}) as cluster_rpc:
            img = Image.open(file)
            img_np = np.array(img).tolist()
            resized_image = cluster_rpc.image_resizer_service.resize_image.call_async(img_np)
            resized_image_converted = np.array(resized_image.result())
            final_resized_image = Image.fromarray(resized_image_converted.astype('uint8'), 'RGB')

        image = filename

        return HTTPStatus.OK


# Method to call the Nameko service
